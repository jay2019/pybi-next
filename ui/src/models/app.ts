import testingData from "@/tempData/configFromPy.json";
import { Container } from "./containers";
import { TDataSource, TDataView, TDataViewBase } from "./dataSources";


export interface App extends Container {
    dataSources: TDataSource[]
    dataViews: TDataViewBase[]
    dbFile: string
    dbLocalStorage: boolean
    echartsRenderer: 'canvas' | 'svg'
}


export function getApp() {
    let config: App | null | string = null

    if (import.meta.env.PROD) {
        config = "__{{__config_data__}}___";
    } else {
        config = testingData as App
    }

    return config
}