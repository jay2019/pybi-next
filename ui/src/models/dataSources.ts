
export interface StructureInfos {
    field: string
    type: string
    values: (string | number | Date)[]

}




export type TDataSource = {
    name: string
}


export type TDataViewBase = {
    name: string
    type: 'sql' | 'pivot'
    excludeLinkages: string[]
}


export type TDataView = TDataViewBase & {
    sql: string
}

export type TPivotDataView = TDataViewBase & {
    source: string
    pivotOptions: {
        row: string
        column: string
        cell: string
        agg: string
        excludeRowFields: boolean
    }
}
