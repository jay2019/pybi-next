import { Statement } from "./statements"


export enum ComponentTag {
    App = "App",
    DataSource = "DataSource",
    Slicer = "Slicer",
    Table = "Table",
    Box = "Box",
    ColBox = "ColBox",
    GridBox = "GridBox",
    FlowBox = "FlowBox",
    EChart = "EChart",
    Upload = 'Upload',
    TextValue = "TextValue",
    Tabs = "Tabs",
}

const containerTags = new Set<ComponentTag>([
    ComponentTag.App, ComponentTag.Box, ComponentTag.ColBox,
    ComponentTag.FlowBox, ComponentTag.GridBox])

export function isContainer(tag: ComponentTag) {
    return containerTags.has(tag)
}



export interface Component {
    id: string
    tag: ComponentTag
    styles: Record<string, any>
    visible?: boolean | {
        sql: string
        mappings: string[]
    }
    gridArea?: string
}


export interface TextComponent extends Component {
    content: string
}

export interface UploadComponent extends Component {

}
