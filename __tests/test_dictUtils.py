import _imports
import pytest
import pybi.utils.dictUtils as dictUtils


def test_get_by_paths_with_index():
    dict_data = {"series": [{"data": [1, 2, 3, 4]}]}

    paths = "series[0].data".split(".")
    act = dictUtils.get_by_paths(paths, dict_data)

    exp = [1, 2, 3, 4]

    assert act == exp


def test_set_by_paths():
    dict_data = {"series": [{"data": [1, 2, 3, 4]}]}
    dictUtils.set_by_paths(["series[0]", "data"], dict_data, "changed")

    assert dict_data["series"][0]["data"] == "changed"


def test_set_by_paths_with_index():
    dict_data = {"series": [{"data": [1, 2, 3, 4]}]}
    dictUtils.set_by_paths(["series[0]"], dict_data, "changed")

    assert dict_data["series"][0] == "changed"
